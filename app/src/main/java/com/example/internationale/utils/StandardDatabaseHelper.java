package com.example.internationale.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.internationale.model.Note;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class StandardDatabaseHelper extends SQLiteOpenHelper {

    private final Context context;

    public static class TableNote {
        public static final String TABLE_NAME = "note";

        public static final String COLUMN_ID       = "id";
        public static final String COLUMN_NAME     = "name";
        public static final String COLUMN_DATE     = "date";
        public static final String COLUMN_LOCATION = "location";
        public static final String COLUMN_WEATHER  = "weather";
        public static final String COLUMN_TEXT     = "text";

        public static final String CREATE_QUERY = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ( " +
                    COLUMN_ID       + " INTEGER PRIMARY KEY NOT NULL, " +
                    COLUMN_NAME     + " TEXT, "                         +
                    COLUMN_DATE     + " TEXT, "                         +
                    COLUMN_LOCATION + " TEXT, "                         +
                    COLUMN_WEATHER  + " TEXT, "                         +
                    COLUMN_TEXT     + " TEXT  "                         +
                ");";

        public static final String SELECT_ONE_BY_ID_QUERY = COLUMN_ID + " = ?";
        public static final String SELECT_ALL_QUERY = "SELECT * FROM " + TABLE_NAME;

        public static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String DELETE_ALL_QUERY = "DELETE FROM " + TABLE_NAME;
    }

    public StandardDatabaseHelper(@Nullable Context context) {
        super(context, "Internationale.db", null, 30);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TableNote.CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(TableNote.DROP_QUERY);
        onCreate(sqLiteDatabase);
    }

    public int addNote() {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        String name = "Новая заметка";
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm:ss"));
        String location = "";
        String weather = "";
        String text = "";

        cv.put(TableNote.COLUMN_NAME, name);
        cv.put(TableNote.COLUMN_DATE, date);
        cv.put(TableNote.COLUMN_LOCATION, location);
        cv.put(TableNote.COLUMN_WEATHER, weather);
        cv.put(TableNote.COLUMN_TEXT, text);

        long id = db.insert(TableNote.TABLE_NAME, null, cv);

        if (id == -1) {
            Toast.makeText(context, "Заметка не может быть добавлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка добавлена", Toast.LENGTH_LONG).show();
            return (int) id;
        }

        return -1;
    }

    public Note getNoteById(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Note note = null;

        if (db != null) {
            Cursor cursor = db.query(
                    TableNote.TABLE_NAME,
                    null,
                    TableNote.SELECT_ONE_BY_ID_QUERY,
                    new String[]{String.valueOf(id)},
                    null,
                    null,
                    null
            );

            if (cursor.getCount() == 0) {
                Toast.makeText(
                            context,
                            "Заметка с id [" + id + "] не найдена",
                            Toast.LENGTH_LONG
                        ).show();
            } else {
                cursor.moveToNext();

                String name = cursor.getString(cursor.getColumnIndexOrThrow(
                        TableNote.COLUMN_NAME
                ));
                String date = cursor.getString(cursor.getColumnIndexOrThrow(
                        TableNote.COLUMN_DATE
                ));
                String location = cursor.getString(cursor.getColumnIndexOrThrow(
                        TableNote.COLUMN_LOCATION
                ));
                String weather = cursor.getString(cursor.getColumnIndexOrThrow(
                        TableNote.COLUMN_WEATHER
                ));
                String text = cursor.getString(cursor.getColumnIndexOrThrow(
                        TableNote.COLUMN_TEXT
                ));

                note = new Note(id, name, date, location, weather, text);
            }

            cursor.close();
        }

        return note;
    }

    public List<Note> getAllNotes() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Note> notes = null;

        if (db != null) {
            Cursor cursor = db.rawQuery(TableNote.SELECT_ALL_QUERY, null);

            if (cursor.getCount() == 0) {
                Toast.makeText(context, "Нет задач", Toast.LENGTH_SHORT).show();
            } else {
                notes = new ArrayList<>();
                while (cursor.moveToNext()) {
                    notes.add(new Note(
                            cursor.getInt(cursor.getColumnIndexOrThrow(TableNote.COLUMN_ID)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TableNote.COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TableNote.COLUMN_DATE)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TableNote.COLUMN_LOCATION)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TableNote.COLUMN_WEATHER)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TableNote.COLUMN_TEXT))
                    ));
                }
            }

            cursor.close();
        }

        return notes;
    }

    public Note updateNoteNameById(int id, String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableNote.COLUMN_NAME, name);

        long result_id = db.update(
                TableNote.TABLE_NAME,
                cv,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не обновлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка обновлена", Toast.LENGTH_LONG).show();
            return getNoteById(id);
        }

        return null;
    }

    public Note updateNoteWeatherById(int id, String weather) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableNote.COLUMN_WEATHER, weather);

        long result_id = db.update(
                TableNote.TABLE_NAME,
                cv,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не обновлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка обновлена", Toast.LENGTH_LONG).show();
            return getNoteById(id);
        }

        return null;
    }

    public Note updateNoteLocationById(int id, String location) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableNote.COLUMN_LOCATION, location);

        long result_id = db.update(
                TableNote.TABLE_NAME,
                cv,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не обновлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка обновлена", Toast.LENGTH_LONG).show();
            return getNoteById(id);
        }

        return null;
    }

    public Note updateNoteTextById(int id, String text) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableNote.COLUMN_TEXT, text);

        long result_id = db.update(
                TableNote.TABLE_NAME,
                cv,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не обновлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка обновлена", Toast.LENGTH_LONG).show();
            return getNoteById(id);
        }

        return null;
    }

    public Note updateNoteById(int id, Note note) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableNote.COLUMN_NAME, note.getName());
        cv.put(TableNote.COLUMN_WEATHER, note.getWeather());
        cv.put(TableNote.COLUMN_LOCATION, note.getLocation());
        cv.put(TableNote.COLUMN_TEXT, note.getText());

        long result_id = db.update(
                TableNote.TABLE_NAME,
                cv,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не обновлена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка обновлена", Toast.LENGTH_LONG).show();
            return new Note(id, note);
        }

        return null;
    }

    public void deleteNoteById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        long result_id = db.delete(
                TableNote.TABLE_NAME,
                TableNote.SELECT_ONE_BY_ID_QUERY,
                new String[]{String.valueOf(id)}
        );

        if (result_id == -1) {
            Toast.makeText(context, "Заметка не удалена", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Заметка удалена", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteAllNotes() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(TableNote.DELETE_ALL_QUERY);
    }
}
