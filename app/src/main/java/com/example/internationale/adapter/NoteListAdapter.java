package com.example.internationale.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.internationale.R;
import com.example.internationale.activity.NoteActivity;
import com.example.internationale.activity.NoteListActivity;
import com.example.internationale.model.Note;
import com.example.internationale.utils.StandardDatabaseHelper;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>{

    private Activity activity;
    private Context context;
    private List<Note> noteList;

    public NoteListAdapter(Activity activity, Context context, List<Note> noteList) {
        this.activity = activity;
        this.context = context;
        this.noteList = noteList;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View noteView = LayoutInflater.from(context).inflate(R.layout.notes_item, parent, false);
        return new NoteViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        Note note = noteList.get(position);

        holder.tvName.setText(note.getName());
        holder.tvDate.setText(note.getDate());

        if (note.getLocation() != null && !note.getLocation().isEmpty()) {
            holder.tvWeather.setText(note.getWeather());
            holder.tvLocation.setText(note.getLocation());
        } else {
            holder.tvWeather.setText("Погода не задана");
            holder.tvLocation.setText("Локация не задана");

            holder.tvWeather.setTextColor(activity.getColor(R.color.grey));
            holder.tvLocation.setTextColor(activity.getColor(R.color.grey));
        }

        if (holder.note_layout != null) {
            holder.note_layout.setOnClickListener((view) -> {
                Intent intent = new Intent(context, NoteActivity.class);
                intent.putExtra("note_id", note.getId());
                activity.startActivity(intent);
            });

            holder.note_layout.setOnLongClickListener((view) -> {
                showDeleteDialog(note);
                return true;
            });
        }
    }

    @Override
    public int getItemCount() {
        if (noteList != null) {
            return noteList.size();
        }
        return 0;
    }

    public void showDeleteDialog(Note note) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();

        dialog.setTitle("Удалить '"+note.getName()+"'?");

        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StandardDatabaseHelper dbh = new StandardDatabaseHelper(context);
                dbh.deleteNoteById(note.getId());
                ((NoteListActivity) activity).loadAdapter();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();
    }

    public static final class NoteViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvDate;
        TextView tvWeather;
        TextView tvLocation;

        ConstraintLayout note_layout;

        public NoteViewHolder(@NonNull View noteView) {
            super(noteView);

            tvName     = (TextView) noteView.findViewById(R.id.note_item_name);
            tvDate     = (TextView) noteView.findViewById(R.id.note_item_date);
            tvWeather  = (TextView) noteView.findViewById(R.id.note_item_weather);
            tvLocation = (TextView) noteView.findViewById(R.id.note_item_location);

            note_layout = (ConstraintLayout) noteView.findViewById(R.id.note_item_layout);
        }

    }

}
