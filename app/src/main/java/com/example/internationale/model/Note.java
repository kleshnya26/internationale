package com.example.internationale.model;

public class Note {

    private Integer id;

    private String name;

    private String date;

    private String location;

    private String weather;

    private String text;

    public Note(Integer id, String name, String date, String location, String weather, String text) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.location = location;
        this.weather = weather;
        this.text = text;
    }

    public Note(Integer id, Note note) {
        this.id = id;
        this.name = note.getName();
        this.date = note.getDate();
        this.location = note.getLocation();
        this.weather = note.getWeather();
        this.text = note.getText();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public String getWeather() {
        return weather;
    }

    public String getText() {
        return text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public void setText(String text) {
        this.text = text;
    }
}
