package com.example.internationale.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;

import com.example.internationale.R;
import com.yandex.mapkit.MapKitFactory;

public class MainActivity extends AppCompatActivity {

    private static boolean initialized = false;

    protected Button bOpenNoteList;
    protected Button bSettings;
    protected Button bExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapKitFactory.setApiKey("84c680d5-86c3-427f-be1c-927c2bb6a249");

        setContentView(R.layout.activity_main);

        bOpenNoteList = findViewById(R.id.button_openNoteList);
        bSettings = findViewById(R.id.button_settings);
        bExit = findViewById(R.id.button_exit);

        bOpenNoteList.setOnClickListener(v -> openMap());
        bSettings.setOnClickListener(v -> openSettings());
        bExit.setOnClickListener(v -> finish());
    }

    @Override
    protected void onResume() {
        checkPermissions();
        super.onResume();
    }

    protected void openMap() {
        Intent intent = new Intent(this, NoteListActivity.class);
        startActivity(intent);
    }

    protected void openSettings() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void checkPermissions(){
        int PERMISSION_ALL = 1;
        String[] permissions = {
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        if (!hasPermissions(this, permissions)) {
            this.requestPermissions(permissions, PERMISSION_ALL);
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (this.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            recreate();
        }
    }
}