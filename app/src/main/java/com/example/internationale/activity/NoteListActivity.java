package com.example.internationale.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.internationale.R;
import com.example.internationale.adapter.NoteListAdapter;
import com.example.internationale.model.Note;
import com.example.internationale.utils.StandardDatabaseHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class NoteListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FloatingActionButton bAddNote;
    private StandardDatabaseHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);

        dbh = new StandardDatabaseHelper(NoteListActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.taskListRecycler);
        bAddNote = (FloatingActionButton) findViewById(R.id.add_note);

        bAddNote.setOnClickListener((view) -> {
            Intent intent = new Intent(this, NoteActivity.class);
            intent.putExtra("note_id", dbh.addNote());
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        loadAdapter();
        super.onResume();
    }

    public void loadAdapter() {
        List<Note> noteList = dbh.getAllNotes();
        NoteListAdapter adapter = new NoteListAdapter(NoteListActivity.this, this, noteList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

}
