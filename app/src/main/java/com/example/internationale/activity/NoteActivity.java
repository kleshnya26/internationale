package com.example.internationale.activity;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.internationale.R;
import com.example.internationale.model.Note;
import com.example.internationale.utils.StandardDatabaseHelper;

public class NoteActivity extends AppCompatActivity {

    private int noteId;

    private TextView tvName;
    private TextView tvDate;
    private TextView tvLocation;
    private TextView tvWeather;
    private TextView tvText;

    private LinearLayout noteMetaInfo;

    private StandardDatabaseHelper dbh;

    private Note currentNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        noteId = getIntent().getExtras().getInt("note_id");

        dbh = new StandardDatabaseHelper(this);

        currentNote = dbh.getNoteById(noteId);

        tvName = (TextView) findViewById(R.id.note_name);
        tvDate = (TextView) findViewById(R.id.note_date);
        tvLocation = (TextView) findViewById(R.id.note_location);
        tvWeather = (TextView) findViewById(R.id.note_weather);
        tvText = (TextView) findViewById(R.id.note_text);

        noteMetaInfo = (LinearLayout) findViewById(R.id.note_meta_info);

        tvName.setText(currentNote.getName());
        tvDate.setText(tvDate.getText().toString() + " " + currentNote.getDate());
        tvText.setText(currentNote.getText());

        if (currentNote.getLocation() != null && !currentNote.getLocation().isEmpty()) {
            tvWeather.setText(tvWeather.getText().toString() + " " + currentNote.getWeather());
            tvLocation.setText(tvLocation.getText().toString() + " " + currentNote.getLocation());
        } else {
            tvWeather.setText("Погода не задана");
            tvLocation.setText("Локация не задана");

            tvWeather.setTextColor(getColor(R.color.grey));
            tvLocation.setTextColor(getColor(R.color.grey));

            noteMetaInfo.setOnClickListener((view) -> {
                Intent intent = new Intent(this, MapActivity.class);
                intent.putExtra("note_id", noteId);
                startActivity(intent);
            });
        }

        tvName.setOnClickListener((view) -> {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            EditText editField = new EditText(this);
            editField.setText(tvName.getText().toString().trim());
            editField.setTextColor(getColor(R.color.black));
            editField.setSelection(editField.length());

            dialog.setTitle("Название заметки");
            dialog.setView(editField);
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                    (dialogInterface, which) -> {
                        tvName.setText(editField.getText().toString().trim());
                        dbh.updateNoteNameById(noteId, tvName.getText().toString().trim());
                    }
            );

            dialog.show();
        });

        tvText.setOnClickListener((view) -> {
            ViewGroup textLayout = (ViewGroup) findViewById(R.id.note_text_layout);

            EditText editField = new EditText(this);

            editField.setText(tvText.getText().toString().trim());
            editField.setTextColor(getColor(R.color.black));
            editField.setSelection(editField.length());
            editField.setVerticalScrollBarEnabled(true);
            editField.setLayoutParams(new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.MATCH_PARENT
            ));
            editField.setInputType(EditorInfo.TYPE_TEXT_FLAG_IME_MULTI_LINE |
                                   EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE |
                                   EditorInfo.TYPE_TEXT_VARIATION_LONG_MESSAGE);
            editField.setSingleLine(false);
            editField.setImeActionLabel("OK", EditorInfo.IME_ACTION_DONE);
            editField.setImeOptions(EditorInfo.IME_ACTION_DONE);

            editField.setOnEditorActionListener((tv, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    tvText.setText(editField.getText().toString().trim());

                    textLayout.removeView(editField);
                    textLayout.addView(tvText);
                    tvText.setVisibility(View.VISIBLE);
                    dbh.updateNoteTextById(noteId, tvText.getText().toString().trim());
                    return true;
                }
                return false;
            });

            textLayout.removeView(tvText);
            textLayout.addView(editField);
            editField.requestFocus();

            InputMethodManager keyboard =
                    (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(editField, InputMethodManager.SHOW_IMPLICIT);
        });
    }

    @Override
    protected void onResume() {
        currentNote = dbh.getNoteById(noteId);

        if (currentNote.getWeather() == null || currentNote.getWeather().isEmpty()) {
            tvWeather.setText("Погода не задана");
        } else {
            tvWeather.setText(currentNote.getWeather());
            tvWeather.setTextColor(getResources().getColor(R.color.black));
        }

        if (currentNote.getLocation() == null || currentNote.getLocation().isEmpty()) {
            tvLocation.setText("Локация не задана");
        } else {
            tvLocation.setText(currentNote.getLocation());
            tvLocation.setTextColor(getResources().getColor(R.color.black));
        }

        super.onResume();
    }

    private void updateNote() {
        currentNote.setName(tvName.getText().toString().trim());
        currentNote.setLocation(tvLocation.getText().toString().trim());
        currentNote.setWeather(tvWeather.getText().toString().trim());
        currentNote.setText(tvText.getText().toString().trim());
        dbh.updateNoteById(noteId, currentNote);
    }

}
